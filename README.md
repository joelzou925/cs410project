## UIUC CS 410 Project by Joel Zou

### Text Classifier Web Platform
This program is developed and tested in Ubuntu 18.04 LTS environment.

The backend is developed with Python 3.6.5 with Django2 and Pytorch1.0 frameworks. 

The frontend is developped in HTML/CSS/Javascript with Boostrap, D3.js and Jquery libraries.

There are 3 versions of the classifier architectures available with customizable parameters (implementation in classifier/api/utils.py):

1. Support Vector Machine (Vapnik et al.)
2. Convolutional Neural Network  (LeCun et al.)
3. LSTM Recurrent Neural Network (Hochreiter et al.)

To train a model, please put training data in a CSV format with one column that contains raw text for each sample and another column that contains the corresponding labels the model should predict.

This repository provides two ready to use datasets for training classifier models.
1. movie_reviews.csv which is a reformatted version of [Stanford Large Movie Reviews dataset](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=2ahUKEwiN-KfG5p_fAhVNq1kKHXnqAYMQFjAAegQIChAB&url=http%3A%2F%2Fai.stanford.edu%2F~amaas%2Fdata%2Fsentiment%2F&usg=AOvVaw1GB7KgKjkRQuTBsVT4zZvz)
2. newsgroups.csv which is a reformatted version of [20 news group text dataset](https://scikit-learn.org/0.19/datasets/twenty_newsgroups.html)

Please make sure to specify the column names used in the csv for the text column and label column when uploading the file, otherwise the program will not able to find the correct data to train on.

After models are  trained, they can be mounted on the Prediction page, and then user can enter text in the provided text area, then model to output the predicted label.


#### Get Started:
##### 1 . Setup Environment
`python -m venv <dir>`

`source <dir>/bin/activate`

##### 2. Install dependencies
`pip install -r requirements.txt`

##### 3. Start Server
`cd classifier`
`python manage.py runserver`

##### 4. Open in browser
###### By default the server will be running on 127.0.0.1:8000
