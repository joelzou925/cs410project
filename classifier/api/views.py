from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.http import JsonResponse, HttpResponse

from api.utils import *
import sqlite3


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print(device)

conn = sqlite3.connect('models.db', check_same_thread=False)
cursor = conn.cursor()
cursor.execute("""
    CREATE TABLE IF NOT EXISTS model (
        name TEXT PRIMARY KEY,
        type TEXT,
        wi BLOB,
        li BLOB,
        config BLOB,
        state_dict BLOB
    )
""")

cursor.close()


model = None
wi = None
li = None

# Create your views here.
class TrainView(TemplateView):
    template_name = 'api/train.html'

    def post(self, request):
        print('received request')
        model_name = request.POST['model_name']
        model_type = request.POST['model_type']

        # process training file
        try:
            # create dataframe 
            train_df = read_csv(request.FILES['train_file'], request.POST['text_col'], request.POST['label_col'])
            train_df['tokens'] = train_df['texts'].apply(tokenize_text)

            # get word indices
            wi = WordIndexer(int(request.POST['max_words']))
            wi.fit(train_df['tokens'])
            train_df['words'] = train_df['tokens'].apply(wi.transform)

            # get label indices
            li = LabelIndexer()
            li.fit(train_df['labels'])
            train_df['labels'] = train_df['labels'].apply(li.transform)

            # create train and validation sets
            validation_fraction = float(request.POST['val_size'])
            train_len = int(len(train_df) * (1 - validation_fraction))
            val_len = len(train_df) - train_len
            train_dataset = Dataset(train_df)
            train_dataset, val_dataset = data.random_split(train_dataset, [train_len, val_len])
            train_loader = data.DataLoader(train_dataset, batch_size=32, shuffle=True, collate_fn=build_batch)
            val_loader = data.DataLoader(val_dataset, batch_size=32, shuffle=False, collate_fn=build_batch)

        except Exception as e:
            print(str(e))
            return render(request, 'api/train.html', {'error': 'Failed Processing File'})


        # get model config
        try:
            if model_type == 'svm':
                config = {
                    'Vword': len(wi.word2idx),
                    'Dword': 50,
                    'output_size': len(li.label2idx)
                }
                print('training svm')
                model = SVMClassifier(config)

            elif model_type == 'cnn':
                config = {
                    'Vword': len(wi.word2idx),
                    'Dword': 50,
                    'kernel_size': int(request.POST['kernel_size']),
                    'num_filters': int(request.POST['num_filters']),
                    'hidden_size': int(request.POST['hidden_size']),
                    'output_size': len(li.label2idx),
                    'dropout': float(request.POST['dropout'])
                }
                print('training cnn')
                model = CNNClassifier(config)

            else:
                config = {
                    'Vword': len(wi.word2idx),
                    'Dword': 50,
                    'rnn_hidden_size': int(request.POST['hidden_size']),
                    'rnn_layers': 1,
                    'output_size': len(li.label2idx),
                    'dropout': float(request.POST['dropout'])
                }
                print('training rnn')
                model = RNNClassifier(config)
        except Exception as e:
            print(str(e))
            return render(request, 'api/train.html', {'error': 'Failed Building Model'})

        # load word embedding
        try:
            Wemb = np.random.randn(len(wi.word2idx), 50).astype(np.float32)
            Wemb[0] = 0
            with open('glove.6B.50d.txt') as f:
                for line in f:
                    word, *emb = line.split(' ')
                    if word in wi.word2idx:
                        Wemb[wi.word2idx[word]] = np.asarray(emb, dtype=np.float32)

            model.load_word_embedding(Wemb)
            model.to(device)
            optimizer = torch.optim.Adam(model.parameters(),
                                        lr=float(request.POST['lr']), 
                                        weight_decay=float(request.POST['reg']))

            # train model
            train_accs, val_accs = train(model, optimizer, 
                                        train_loader, val_loader, 
                                        lr_decay=float(request.POST['decay']),
                                        epochs=int(request.POST['epochs']))
        except Exception as e:
            print(str(e))
            return render(request, 'api/train.html', {'error': 'Failed Training Model'})


        # save model
        cursor = conn.cursor()
        state_dict = model.state_dict()
        cursor.execute("""
            INSERT OR REPLACE INTO model (name, type, wi, li, config, state_dict)
            VALUES (?, ?, ?, ?, ?, ?)
        """, (model_name,
                model_type,
                pickle.dumps(wi),
                pickle.dumps(li), 
                pickle.dumps(config),
                pickle.dumps(state_dict)))
        conn.commit()


        context = {'train_accs': train_accs,
                   'val_accs': val_accs,
                   'model_name': model_name,
                   'model_type': model_type}
        return render(request, 'api/train.html', context)


@method_decorator(csrf_exempt, name='dispatch')
class PredictView(TemplateView):
    template_name = 'api/predict.html'

    def get(self, request):
        cursor = conn.cursor()
        r = cursor.execute("""
        SELECT name from model
        """)
        models = [row[0] for row in r]
        context = {
            'models': models
        }
        return render(request, 'api/predict.html', context)

    def post(self, request, format=None):
        texts = [request.POST['text']]

        test_df = pd.DataFrame({'texts': texts})
        test_df['tokens'] = test_df['texts'].apply(tokenize_text)
        test_df['words'] = test_df['tokens'].apply(wi.transform)
        test_dataset = Dataset(test_df)
        test_loader = data.DataLoader(test_dataset, batch_size=32, shuffle=False, collate_fn=build_batch)

        ypred = predict(model, test_loader)[0]

        return JsonResponse({'result': li.idx2label[ypred]})


@method_decorator(csrf_exempt, name='dispatch')
class LoadModelView(TemplateView):
    template_name = 'api/predict.html'

    def post(self, request):
        global model
        global wi
        global li
        model_name = request.POST['model']
        cursor = conn.cursor()

        r = cursor.execute("""
        SELECT * from model where name = ?
        """, (model_name,))
        result = r.fetchall()[0]
        model_type = result[1]
        wi = pickle.loads(result[2])
        li = pickle.loads(result[3])
        config = pickle.loads(result[4])
        state_dict = pickle.loads(result[5])


        if model_type == 'svm':
            model = SVMClassifier(config)
            print('loaded svm')

        elif model_type == 'cnn':
            model = CNNClassifier(config)
            print('loaded cnn')

        else:
            model = RNNClassifier(config)
            print('loaded rnn')

        model.load_state_dict(state_dict)
        model.to(device)
        return JsonResponse({'message': 'success'})