from django.urls import path
from api import views

urlpatterns = [
    path('', views.TrainView.as_view(), name='train'),
    path('predict/', views.PredictView.as_view(), name='predict'),
    path('load/', views.LoadModelView.as_view(), name='load'),
]
