import numpy as np
import torch
import torch.nn as nn
import torch.utils.data as data
import pickle
import time
import pandas as pd
import unicodedata
import re
from sklearn.metrics import classification_report
from collections import Counter


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

## Data utilities

class WordIndexer:
    def __init__(self, max_words=100000):
        self.max_words= max_words
        
    def fit(self, data):
        word_counter = Counter()
        for line in data:
            for word in line:
                word_counter.update([word])
                
        words = ['<pad>', '<unk>']
        words += [word for word, count in word_counter.most_common() if word not in words]
        words = words[:self.max_words]
        
        self.word2idx = {w: i for i, w in enumerate(words)}
        self.idx2word = {i: w for i, w in enumerate(words)}
        
    def transform(self, tokens):
        result = []
        for token in tokens:
            result.append(self.word2idx[token] if token in self.word2idx else self.word2idx['<unk>'])
        return result

class LabelIndexer:
    def fit(self, data):
        labels = set(data)
        self.label2idx = {l: i for i, l in enumerate(labels)}
        self.idx2label = {i: l for i, l in enumerate(labels)}

    def transform(self, label):
        return self.label2idx[label] 


class Dataset(data.Dataset):
    def __init__(self, df):
        self.df = df
        
    def __len__(self):
        return len(self.df)
    
    def __getitem__(self, i):
        i = int(i)
        return self.df.iloc[i:i+1]

def read_csv(fname, text_col, label_col):  
    df = pd.read_csv(fname)
    inds = np.random.permutation(len(df))
    new_df = pd.DataFrame()
    new_df['texts'] = df[text_col].astype(str)
    new_df['labels'] = df[label_col].astype(str)
    return new_df.reset_index(drop=True)

def tokenize_text(text):
    text = unicodedata.normalize('NFKD', text).encode('ascii', 'ignore').decode('utf-8', 'ignore')
    text = re.sub(r'[^A-Za-z0-9,.?!\']', ' ', text)
    text = re.sub(r'\d+', '0', text)
    text = re.sub(r'\s+', ' ', text)
    text = text.lower().strip()
    return text.split()


def build_batch(batch):
    df = pd.concat(batch)
    batch_words = torch.tensor(pad_sequences(df['words'].tolist())).to(device)
    if 'labels' in df:
        batch_labels = torch.tensor(df['labels'].tolist()).to(device)
        return batch_words, batch_labels
    return batch_words

def acc(ypred, ytrue):
    acc = torch.mean((ypred == ytrue).type(torch.float32))
    return acc


## Models

class SVMClassifier(nn.Module):
    def __init__(self, config):
        super(SVMClassifier, self).__init__()
        self.word_embedding = nn.Embedding (config['Vword'], config['Dword'], padding_idx=0)
        self.fc = nn.Linear(config['Dword'], config['output_size'])
    
    def forward(self, words):
        words = self.word_embedding(words)
        words = torch.mean(words, dim=1)
        out = self.fc(words)
        return out
        
    def load_word_embedding(self, weight):
        self.word_embedding.weight = nn.Parameter(torch.tensor(weight))    
    
    def loss(self, logits, labels):
        error = 1 - (logits[torch.arange(len(labels)), labels].unsqueeze(-1) - logits)  # (N, O)
        error = torch.clamp(error, 0)
        error[torch.arange(len(labels)), labels] = 0
        loss = torch.sum(error) / len(labels)
        return loss


class CNNClassifier(nn.Module):
    def __init__(self, config):
        super(CNNClassifier, self).__init__()
        padding = (config['kernel_size'] -1) // 2   # keep same dimension
        
        self.word_embedding = nn.Embedding(config['Vword'], config['Dword'], padding_idx=0)
        self.conv1 = nn.Conv1d(config['Dword'], config['num_filters'], kernel_size=config['kernel_size'], stride=1, padding=padding)
        self.pool1 = nn.MaxPool1d(kernel_size=config['kernel_size'])
        self.conv2 = nn.Conv1d(config['num_filters'], config['num_filters'], kernel_size=config['kernel_size'], stride=1, padding=padding)
        self.pool2 = nn.MaxPool1d(kernel_size=config['kernel_size'])
        self.conv3 = nn.Conv1d(config['num_filters'], config['num_filters'], kernel_size=config['kernel_size'], stride=1, padding=padding)
        self.fc1 = nn.Linear(config['num_filters'], config['hidden_size'])
        self.fc2 = nn.Linear(config['hidden_size'], config['output_size'])
        self.dropout = nn.Dropout(config['dropout'])
        
        
    def load_word_embedding(self, weight):
        self.word_embedding.weight = nn.Parameter(torch.tensor(weight))    
    
    def forward(self, words):
        # encoding
        words = self.word_embedding(words)   # (N, T, Dword)
        words = self.dropout(words)
        
        # conv block
        x = torch.transpose(words, 1, 2)   # (N, Dword, T)
        x = torch.relu(self.conv1(x))   # (N, Dword, T)  
        x = self.pool1(x)
        x = self.dropout(x)
        x = torch.relu(self.conv2(x))
        x = self.pool2(x)
        x = self.dropout(x)
        x = torch.relu(self.conv3(x))
        x, _ = torch.max(x, dim=-1)   # (N, Dc)
        
        x = self.fc1(x)
        x = self.dropout(x)
        out = self.fc2(x)
        return out
    
    def loss(self, logits, labels):
        criterion = nn.CrossEntropyLoss()
        return criterion(logits, labels)


class RNNClassifier(nn.Module):
    def __init__(self, config):
        super(RNNClassifier, self).__init__()
        self.word_embedding = nn.Embedding(config['Vword'], config['Dword'], padding_idx=0)
        self.rnn = nn.LSTM(config['Dword'], config['rnn_hidden_size'], 
                           num_layers=config['rnn_layers'],
                            batch_first=True, bidirectional=True,
                          dropout=config['dropout'] if config['rnn_layers'] > 1 else 0)
        
        self.fc1 = nn.Linear(config['rnn_hidden_size']*2, 1, bias=False)
        self.fc2 = nn.Linear(config['rnn_hidden_size']*2, config['output_size'])
        self.dropout = nn.Dropout(config['dropout'])
    
    def forward(self, words):
        mask = words.detach().gt(0).float()
        
        # encoding
        words = self.word_embedding(words)  # (N, T, Dword) 
        words = self.dropout(words)
        x, state = self.rnn(words)   # (N, T, H)

        context_vector, _ = torch.max(x * mask.unsqueeze(-1), dim=1)  # (N, H)
        context_vecotr = torch.tanh(context_vector)
        context_vector = self.dropout(context_vector)
        out = self.fc2(context_vector)
        return out
            
    def load_word_embedding(self, weight):
        self.word_embedding.weight = nn.Parameter(torch.tensor(weight))    
        
    def loss(self, logits, labels):
        criterion = nn.CrossEntropyLoss()
        return criterion(logits, labels)



def pad_sequences(sequences):
    max_sent_len = 0

    for sent in sequences:
        max_sent_len = max(len(sent), max_sent_len)

    padded = np.zeros((len(sequences), max_sent_len), dtype=int)

    for i, sent in enumerate(sequences):
        padded[i, :len(sent)] = sent
    
    return padded




## Training and Testing Functions

def train(model, optimizer, train_loader, val_loader, epochs=10, lr_decay=0.99):
    train_accs = []
    val_accs = []

    for e in range(epochs):
        model.train()
        train_loss = 0
        train_acc = 0
        train_counter = 0
        
        for words, labels in train_loader:
            logits = model(words)
            loss = model.loss(logits, labels)
            
            optimizer.zero_grad()
            loss.backward()
            nn.utils.clip_grad_norm_(model.parameters(), 5.0)
            optimizer.step()
            
            train_loss += loss.detach()
            train_acc += acc(logits.argmax(-1).detach(), labels.detach())
            train_counter += 1

        model.eval()
        val_loss = 0
        val_acc = 0
        val_counter = 0
        
        for words, labels in val_loader:
            with torch.no_grad():
                logits = model(words)
                loss = model.loss(logits, labels)

                val_loss += loss.detach()
                val_acc += acc(logits.argmax(-1).detach(), labels.detach())
                val_counter += 1
        
        train_accs.append(train_acc.tolist() / train_counter)
        val_accs.append(val_acc.tolist() / val_counter)
        print('Epoch {}, Train Loss {}, Train Acccuracy {}, Val Loss {} Val Accuracy {}'.format(e+1, train_loss / train_counter, train_acc / train_counter, 
                                                                                                val_loss / val_counter, val_acc / val_counter))
        for g in optimizer.param_groups:
            g['lr'] *= lr_decay
    
    return train_accs, val_accs    
    


def predict(model, test_loader):
    model.eval()
    results = []
    
    for words in test_loader:
        with torch.no_grad():
            logits = model(words)
            results.extend(logits.argmax(-1).tolist())
    return results



